package com.itau.maratona;

import java.util.ArrayList;

public class Equipe {
	public int id;
	public ArrayList<Aluno> alunos = new ArrayList<Aluno>();
	
	public String toString()
	{
		String retorno = "";
		retorno += id + " - \n";
		
		for (Aluno aluno : alunos)
		{
			retorno += aluno.nome;
			retorno += "\n";
		}
		
		return retorno;
	}
}
