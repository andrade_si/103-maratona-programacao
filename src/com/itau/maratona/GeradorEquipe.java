package com.itau.maratona;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GeradorEquipe {

	public static List<Equipe>  GerarEquipes(List<Aluno> pListaAlunos) {
		int indexRandomico = 0;
        int idEquipe = 1;
        Random random = new Random();
        
        //Lista completa de equipes
		List<Equipe> listaEquipe = new ArrayList<>();

		// Enquanto a lista tiver alunos...
		while (pListaAlunos.size() > 0) {

			Equipe objEquipe = new Equipe();
			objEquipe.id = idEquipe;
			
			int vezes = 3;
			if (pListaAlunos.size() >= 3)
			{
				vezes = 3;
			}
			else
			{
				vezes = pListaAlunos.size();
			}
			
			//Laço com 3 porque o máximo de alunos definido por equipe é 3
			for (int i = 1; i <= vezes; i++) {
			
				// Gera um índice randômico
				indexRandomico = random.nextInt(pListaAlunos.size());

				// Obtem dados do aluno selecionado e adiciona na equipe
				objEquipe.alunos.add(pListaAlunos.get(indexRandomico));
				
				//Remover o aluno da lista de alunos para não ser obtido novamente (evitar repetição)
				pListaAlunos.remove(indexRandomico);
			}
			
			//Adiciona equipe na lista
			listaEquipe.add(objEquipe);
			
			//Incrementa o id da equipe
			idEquipe += 1;
		}

		return listaEquipe;
	}
}
