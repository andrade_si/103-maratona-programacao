package com.itau.maratona;

public class Aluno {
	public String nome;
	public String cpf;
	public String email;

	public String toString() {
		return nome + " - CPF: " + cpf + " (" + email + ")";
	}
}
