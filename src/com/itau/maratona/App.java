package com.itau.maratona;

import java.util.List;

public class App {

	public static void main(String[] args) {
		
		//Obtém arquivo local
		Arquivo objArquivo = new Arquivo("src/alunos.csv");
		
		//Gera equipes com base no arquivo de alunos
		List<Equipe> listaEquipe = GeradorEquipe.GerarEquipes(objArquivo.ler());
		
		//Exibe a lista de equipes
		Relatorio.Exibir(listaEquipe);
	}
}
