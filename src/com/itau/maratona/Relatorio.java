package com.itau.maratona;

import java.util.List;

public class Relatorio {

	public static void Exibir(List<Equipe> pListaEquipe) {
		
		//Para cada equipe
		for (Equipe equipe : pListaEquipe) {
			
			//Para cada aluno da equipe
			for (Aluno aluno : equipe.alunos) {
				System.out.println(equipe.id + " - " + aluno);
			}
		}
		
		
		//Método 2
		for (Equipe equipe : pListaEquipe) {
			System.out.println(equipe);
		}
	}
}
